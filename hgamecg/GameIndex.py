import re
import pickle


class GameIndex:
    __slots__ = ['index', 'quantity']
    def __init__(self):
        self.index = {}
        self.quantity = 0

    def save(self, filename):
        pickle.dump(self, open(filename, 'wb'))

    def add(self, item):
        brand, title, url = item
        if brand not in self.index:
            self.index[brand] = []
        self.index[brand].append((title, url))
        self.quantity += 1

    def searchTitle(self, searchString):
        result = []
        for brand, games in self.index.items():
            for game in games:
                title, url = game
                search_part = searchString.split(' ')
                # TODO: Fuzzy search, try to implement it
                for k in search_part:
                    p = re.compile(".*?({}).*?".format(k), flags=re.IGNORECASE)
                    m = p.match(title)
                    if m is None:
                        continue
                    result.append((brand, title, url))
                    break
        return result

    def searchBrand(self, brand):
        pass
