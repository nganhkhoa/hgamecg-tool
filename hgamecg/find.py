from menu import clearScreen, menu
from getkey import getKey


def findGame():
    import config
    INDEX = config.INDEX
    GAME_URL = config.GAME_URL

    buf = ""
    while True:
        clearScreen()
        print("Enter game name to find: {}".format(buf))
        k = getKey()
        if k == '\n':
            break
        if ord(k) == 127:
            buf = buf[:-1]
        else:
            buf = buf + k

    result = INDEX.searchTitle(buf)
    titles = [r[1] for r in result]
    titles.append("Go Back To Main Page")
    while True:
        index = menu("[+] {} results found:".format(len(result)), titles)
        if index == len(titles):
            break
        brand, title, url = result[index - 1]

        # TODO: Make a menu with game option
        clearScreen()
        print("{} game".format(brand))
        print("\t{}\n\t--> {}".format(title, GAME_URL + url))
        input()


def findBrand():
    import config
    clearScreen()
    brandToFind = input("Enter brand to find: ")
    for brand, games in config.INDEX.index.items():
        if brand.lower() != brandToFind.lower():
            continue
        print(f"{brand}:", file=output)
        for game in games:
            title, url = game
            print(f"\t{title}\n\t--> {config.GAME_URL}/{url}", file=output)
