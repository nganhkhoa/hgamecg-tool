import click
from direction import Direction
from platform import system


def linuxkey(k):
    if k == '\033[A':
        return Direction.up
    elif k == '\033[B':
        return Direction.down
    elif k == '\033[C':
        return Direction.right
    elif k == '\033[D':
        return Direction.left
    elif k == ' ':
        return ' '
    elif ord(k) == 13:
        return '\n'

    elif ord(k) == 3:
        raise KeyboardInterrupt

    else:
        return k


def windowskey(k):
    if ord(k) == 72:
        return Direction.up
    elif ord(k) == 80:
        return Direction.down
    elif ord(k) == 77:
        return Direction.right
    elif ord(k) == 75:
        return Direction.left
    elif k == ' ':
        return ' '
    elif ord(k) == 13:
        return '\n'


def mackey(k):
    if k == ' ':
        return ' '
    elif ord(k) == 13:
        return '\n'


def getKey():
    k = click.getchar()

    if system() == "Linux":
        return linuxkey(k)

    elif system() == "Windows":
        return windowskey(k)

    elif system() == "Darwin":
        return mackey(k)

    else:
        return k
