from direction import Direction
from getkey import getKey


def clearScreen():
    print("\033[1H", end="")
    print("\033[J", end="")


def menu(title, selections, choice=1):
    # TODO: Paging
    while True:
        clearScreen()
        print(title)

        for i in range(len(selections)):
            if i == choice - 1:
                print("\033[1;91m> ", end="")
            print("{}. ".format(i + 1), end="")
            print(selections[i])
            if i == choice - 1:
                print("\033[0m", end="")

        key = getKey()

        if key == Direction.up:
            choice = (choice - 2) % len(selections) + 1
        elif key == Direction.down:
            choice = (choice) % len(selections) + 1
        elif key == '\n':
            return choice
        else:
            continue
