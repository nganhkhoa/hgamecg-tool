import sys
import pickle

from menu import menu
from getkey import getKey
from crawl import crawl
from GameIndex import GameIndex
from find import findGame, findBrand


def printIndex(output=sys.stdout):
    import config
    for brand, games in config.INDEX.index.items():
        print(f"{brand}:", file=output)
        for game in games:
            title, url = game
            print(f"\t{title}\n\t--> {config.GAME_URL}{url}", file=output)


if __name__ == "__main__":
    title = "hgamecg.com tool"
    choices = [
        "Create Index (Crawl hgamecg.com)",
        "Search Game",
        "Search Brand",
        "~~Print Index~~ (the index is too large)",
        "Print Index to file",
        "Exit"
    ]
    exitChoice = len(choices)
    loaded = False
    import config
    INDEX_FILE = config.INDEX_FILE

    while True:
        choice = menu(title, choices)
        if choice == exitChoice:
            break

        if choice == 1:
            y = input('Crawl database? (y/n) ')
            if y != 'y' and y != 'Y':
                continue
            config.INDEX = GameIndex()
            crawl()
            print('Crawl completed, press to continue')
            getKey()
            config.INDEX.save(INDEX_FILE)
            loaded = True

        if not loaded:
            try:
                config.INDEX = pickle.load(open(INDEX_FILE, 'rb'))
                loaded = True
            except FileNotFoundError:
                print('No index file found, execute crawl please')
                getKey()
                continue

        if choice == 2:
            findGame()

        if choice == 3:
            findBrand()

        if choice == 4:
            # printIndex()
            print('Because the index is too large, we disable this')
            print('Consider using print to file')
            getKey()

        if choice == 5:
            printIndex(open(input('output file name: '), 'w'))
