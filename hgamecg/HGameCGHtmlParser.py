import re

from html.parser import HTMLParser


class HGameCGHtmlParser(HTMLParser):
    data = []
    isLink = False
    gameLink = ""

    def handle_starttag(self, tag, attrs):
        self.isLink = tag == "a"
        if not self.isLink:
            return

        for item in attrs:
            att, value = item
            if att != "href" or value == '/' or re.match(r'/page-\d+.html', value):
                continue
            self.gameLink = value

    def handle_data(self, data):
        if self.isLink is not True or self.gameLink == "":
            return

        brand = ""
        title = ""
        if (m := re.match(r'\[(.*)\]-(.*)', data)) is None:
            return
        if len(m.group(1)) > len(brand):
            brand, title = m.group(1, 2)
        # print((brand, title, self.gameLink))
        self.data.append((brand.upper(), title, self.gameLink))
