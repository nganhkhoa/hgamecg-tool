import sys
import config

from concurrent.futures import as_completed
from requests_futures.sessions import FuturesSession
from HGameCGHtmlParser import HGameCGHtmlParser


def crawl():
    INDEX = config.INDEX
    SPIDER_URL = config.SPIDER_URL
    PAGE_OFFSET = config.PAGE_OFFSET

    s = FuturesSession()
    currentIndex = 1
    reached_not_found = False
    while not reached_not_found:
        futures = [s.get(SPIDER_URL.format(i), headers={ 'User-Agent': config.USERAGENT }
                        ) for i in range(currentIndex, currentIndex + 10, PAGE_OFFSET)]
        currentIndex += 10
        for future in as_completed(futures):
            r = future.result()
            print('[+]', r.url, r.status_code, file=sys.stderr)
            if r.status_code != 200:
                reached_not_found = True
                continue
            html = r.text
            parser = HGameCGHtmlParser()
            try:
                # print(html)
                parser.feed(html)
            except BaseException as e:
                print(e)
                break

            list(map(INDEX.add, parser.data))
