from enum import Enum


class Direction(Enum):
    up = 1
    down = 2
    right = 3
    left = 4

    def __repr__(self):
        return self.name.upper()

    def __str__(self):
        return self.name.upper()

    def reverse(self):
        if self.value % 2 == 0:
            return Direction(self.value - 1)
        else:
            return Direction(self.value + 1)

    def isReverse(self, m):
        if self.value % 2 == 0:
            return self.value - 1 == m.value
        else:
            return self.value + 1 == m.value
