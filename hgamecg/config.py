from GameIndex import GameIndex

GAME_URL: str = "https://hgamecg.com"
SPIDER_URL: str = "https://hgamecg.com/page-{}.html"
PAGE_OFFSET: int = 1
INDEX_FILE: str = "hgamecg.index"
USERAGENT: str = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
INDEX = GameIndex()
